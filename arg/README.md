### ARG
ARG is to provide the build time variables to Dockerfile. These can't be accessed for the container. ARG is the only instruction we can use before FROM.

### ARG vs ENV
* ARG is for build time
* ENV is to provide environment variables for containers.
* we can use the combination of both for best results by assigning ARG value to ENV so that container can access ARG.