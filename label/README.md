### LABEL
LABEL is to provide the tags as key value pairs. Labels are useful for querying purpose.
```
docker images -f label=environment=dev
```