### CMD
CMD is the instruction runs at the time of creating container. It should make container keeps running.

### Difference b/w RUN and CMD

* RUN runs at the time of image build
* CMD runs at the time of container creation