### What is Dockerfile
Dockerfile is the declarative way of creating custom docker images. Every Dockerfile first instruction should be FROM to refer the Base OS. On top of base OS we install application dependencies, runtime, code, etc.

### How to build docker image
```
docker build -t <repo-name>/<image-name>:<tag> .
```

### How to push docker image to hub
you need to login before you push using docker login

```
docker push <docker-hub-username>/<image-name>:<tag>
```
other external repos
```
docker push <URL>/<username>/<image-name>:<tag>
```