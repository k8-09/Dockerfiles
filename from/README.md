### FROM
FROM is the first instruction you should give in Dockerfile. It refers base OS. There is one exception. ARG can be the first instruction in some cases.

```
docker build -t from:v1 .
```
to push to docker hub
```
docker login
```
```
docker push <your-hub-username>/from:v1
```
