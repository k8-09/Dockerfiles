### CMD vs ENTRYPOINT
* CMD can be override from terminal
* ENTRYPOINT can't be overridden
* We use combination of both CMD and ENTRYPOINT for best results like
    * CMD provides the default arguments to ENTRYPOINT
    * Users can always override CMD for other arguments.